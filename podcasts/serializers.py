from rest_framework import serializers


class EpisodeSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.CharField()
    description = serializers.CharField()
    pub_date = serializers.DateTimeField()
    link = serializers.URLField()
    image = serializers.URLField()
    podcast_name = serializers.CharField()
    guid = serializers.CharField()
