from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from podcasts.models import Episode
from podcasts.serializers import EpisodeSerializer


class HomePageView(APIView):

    def get(self, request):
        episodes = Episode.objects.filter().order_by("-pub_date")[:10]
        serializer = EpisodeSerializer(episodes, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
