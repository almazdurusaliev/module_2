import datetime

from rest_framework import serializers

from blog.models import Category, Post, Comment


class CategorySerailizer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()


class CommentSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    author = serializers.CharField()
    body = serializers.CharField()
    created_at = serializers.DateTimeField()
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all())

    def create(self, validated_data):
        Comment(**validated_data).save()
        return Comment(**validated_data)


class PostSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField()
    body = serializers.CharField()
    created_on = serializers.DateTimeField(default=datetime.datetime.now())
    last_modified = serializers.DateTimeField(default=datetime.datetime.now())
    categories = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all(), many=True)

    def create(self, validated_data):
        Post(**validated_data).save()
        return Post(**validated_data)

    def to_representation(self, instance):
        repr = super().to_representation(instance)
        if instance.comment.exists():
            repr['comments'] = CommentSerializer(instance.comment.all(), many=True).data
        return repr
