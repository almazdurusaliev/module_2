from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema

from blog.models import Post
from blog.serializers import CommentSerializer, PostSerializer


class BlogIndexView(APIView):
    def get(self, request):
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(request_body=PostSerializer)
    def post(self, request):
        post = PostSerializer(data=request.data)
        post.is_valid(raise_exception=True)
        post.save()
        return Response(post.data, status=status.HTTP_201_CREATED)


class BlogDetailView(APIView):
    def get(self, request, pk):
        post = Post.objects.get(pk=pk)
        serializer = PostSerializer(post)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(request_body=CommentSerializer)
    def post(self, request, pk):
        post = Post.objects.get(pk=pk)
        comment = CommentSerializer(data=request.data)
        comment.save(post=post)
        return Response(comment.data, status=status.HTTP_201_CREATED)


class BlogCategoryView(APIView):
    def get(self, request, category):
        posts = Post.objects.filter(categories__name=category)
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)