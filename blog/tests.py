from rest_framework import status
from rest_framework.reverse import reverse_lazy
from rest_framework.test import APITestCase

from blog.models import Post, Comment, Category


class BlogIndexViewTest(APITestCase):
    def test_GET_request(self):
        category = Category(name='First')
        category.save()
        category2 = Category(name='Second')
        category2.save()
        blog = Post(title='random', body='Some random things')
        blog.save()
        blog.categories.set([category2, category])
        request = self.client.get(reverse_lazy('blog_index'))
        self.assertEqual(request.status_code, status.HTTP_200_OK)

    def test_post_request(self):
        pass


class BlogDetailViewTest(APITestCase):
    def test_GET_request(self):
        category = Category(name='First')
        category.save()
        blog = Post(title='random', body='Some random things')
        blog.save()
        blog.categories.set([category])
        request = self.client.get(reverse_lazy('blog_detail', kwargs={'pk': blog.pk}))
        self.assertEqual(request.status_code, status.HTTP_200_OK)

    def test_post_request(self):
        pass


class BlogCategoryViewTest(APITestCase):
    def test_GET_request(self):
        category = Category(name='First')
        category.save()
        category2 = Category(name='Second')
        category2.save()
        blog = Post(title='random', body='Some random things')
        blog.save()
        blog.categories.set([category2, category])
        request = self.client.get(reverse_lazy('blog_category', kwargs={'category': category.name}))
        self.assertEqual(request.status_code, status.HTTP_200_OK)
