from rest_framework import status
from rest_framework.reverse import reverse_lazy
from rest_framework.test import APITestCase

from todo_app.models import ToDoItem, ToDoList


class ListListView(APITestCase):
    def test_GET_request(self):
        request = self.client.get(reverse_lazy('index'))
        self.assertEqual(request.status_code, status.HTTP_200_OK)


class ItemListView(APITestCase):
    def test_GET_request(self):
        request = self.client.get(reverse_lazy('list', kwargs={'list_id': 1}))
        self.assertEqual(request.status_code, status.HTTP_200_OK)


class ListCreate(APITestCase):
    def test_POST_request(self):
        request = self.client.post(reverse_lazy('list-add'), data={'title': 'TEST Title'})
        self.assertEqual(request.status_code, status.HTTP_201_CREATED)
        self.assertEqual(request.data['title'], 'TEST Title')


class ItemCreate(APITestCase):
    def test_POST_request(self):
        todo_list = ToDoList(title='Some Title')
        todo_list.save()
        data = {'title': 'Item Title', 'description': 'Item description',
                'due_date': '2022-12-18T15:38:22.040Z'}
        request = self.client.post(reverse_lazy('item-add', kwargs={'list_id': todo_list.pk}), data=data)
        self.assertEqual(request.status_code, status.HTTP_201_CREATED)


class ItemUpdate(APITestCase):
    def test_PUT_request(self):
        todo_list = ToDoList(title='Some Title')
        todo_list.save()
        item = ToDoItem(title='Some Title', description='Item description',
                        due_date='2022-12-18T15:38:22.040Z', todo_list=todo_list)
        item.save()
        data = {'title': 'Item Put Title', 'description': 'Item Put description',
                'due_date': '2023-12-18T15:38:22.040Z'}
        request = self.client.put(reverse_lazy('item-update',
                                               kwargs={'list_id': todo_list.pk, 'pk': item.pk}),
                                  data=data)
        self.assertEqual(request.status_code, status.HTTP_202_ACCEPTED)


class ListDelete(APITestCase):
    def test_DELETE_request(self):
        todo_list = ToDoList(title='Some Title')
        todo_list.save()
        request = self.client.delete(reverse_lazy('list-delete', kwargs={'pk': todo_list.pk}))
        self.assertEqual(request.status_code, status.HTTP_202_ACCEPTED)


class ItemDelete(APITestCase):
    def test_DELETE_request(self):
        todo_list = ToDoList(title='Some Title')
        todo_list.save()
        item = ToDoItem(title='Some Title', description='Item description',
                        due_date='2022-12-18T15:38:22.040Z', todo_list=todo_list)
        item.save()
        request = self.client.delete(reverse_lazy('item-delete', kwargs={'list_id': todo_list.pk, 'pk': item.pk}))
        self.assertEqual(request.status_code, status.HTTP_202_ACCEPTED)
