from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from todo_app.models import ToDoList, ToDoItem
from todo_app.serializers import ToDoListSerializer, ToDoItemSerializer


class ListListView(APIView):
    def get(self, request):
        lists = ToDoList.objects.all()
        serializer = ToDoListSerializer(lists, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ItemListView(APIView):
    def get(self, request, list_id):
        items = ToDoItem.objects.filter(todo_list_id=list_id)
        serializer = ToDoItemSerializer(items, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ListCreate(APIView):

    @swagger_auto_schema(request_body=ToDoListSerializer)
    def post(self, request):
        serializer = ToDoListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class ItemCreate(APIView):
    @swagger_auto_schema(request_body=ToDoItemSerializer)
    def post(self, request, list_id):
        serializer = ToDoItemSerializer(data=request.data)
        list = ToDoList.objects.get(pk=list_id)
        if serializer.is_valid():
            serializer.save(todo_list=list)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class ItemUpdate(APIView):
    @swagger_auto_schema(request_body=ToDoItemSerializer)
    def put(self, request, list_id, pk):
        item = ToDoItem.objects.get(pk=pk)
        serializer = ToDoItemSerializer(item, data=request.data)
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class ListDelete(APIView):
    def delete(self, request, pk):
        do_list = ToDoList.objects.get(pk=pk)
        do_list.delete()
        return Response(status=status.HTTP_202_ACCEPTED)


class ItemDelete(APIView):
    def delete(self, request, list_id, pk):
        item = ToDoItem.objects.get(pk=pk)
        item.delete()
        return Response(status=status.HTTP_202_ACCEPTED)
