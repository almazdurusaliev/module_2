from rest_framework import serializers

from todo_app.models import ToDoItem, ToDoList


class ToDoListSerializer(serializers.Serializer):
    title = serializers.CharField()

    def create(self, validated_data):
        todo_list = ToDoList(**validated_data)
        todo_list.save()
        return todo_list


class ToDoItemSerializer(serializers.Serializer):
    title = serializers.CharField()
    description = serializers.CharField()
    created_date = serializers.DateTimeField(read_only=True)
    due_date = serializers.DateTimeField()
    todo_list = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        item = ToDoItem(**validated_data)
        item.save()
        return item

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title')
        instance.description = validated_data.get('description')
        instance.due_date = validated_data.get('due_date')
        instance.todo_list = validated_data.get('todo_list')
        instance.save()
        return instance
