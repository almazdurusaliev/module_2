from rest_framework import serializers

from entries.models import Entry


class EntrySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField()
    content = serializers.CharField()
    date_created = serializers.DateTimeField(read_only=True)

    def create(self, validated_data):
        entry = Entry(**validated_data)
        entry.save()
        return entry

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title')
        instance.content = validated_data.get('content')
        instance.save()
        return instance
