from rest_framework import status
from rest_framework.reverse import reverse_lazy
from rest_framework.test import APITestCase

from entries.models import Entry


class EntryListViewTest(APITestCase):
    def test_GET_request(self):
        request = self.client.get(reverse_lazy('entry-list'))
        self.assertEqual(request.status_code, status.HTTP_200_OK)


class EntryDetailViewTest(APITestCase):
    def test_GET_request(self):
        entry = Entry(title='Test', content='Content Test')
        entry.save()
        request = self.client.get(reverse_lazy('entry-detail', kwargs={'pk': 1}))
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertContains(request, 'Content Test')


class EntryCreateViewTest(APITestCase):
    def test_POST_request(self):
        data = {'title': 'Test Title', 'content': 'Some random content'}
        request = self.client.post(reverse_lazy('entry-create'), data=data)
        self.assertEqual(request.status_code, status.HTTP_201_CREATED)
        self.assertEqual(request.data['content'], 'Some random content')


class EntryUpdateView(APITestCase):
    def test_POST_request(self):
        entry = Entry(title='Test', content='Content Test')
        entry.save()
        request = self.client.put(reverse_lazy('entry-update', kwargs={'pk': 1}),
                                  data={'title': 'Test', 'content': 'Put Content Test'})
        self.assertEqual(request.status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(request.data['content'], 'Put Content Test')


class EntryDeleteView(APITestCase):
    def test_DELETE_request(self):
        entry = Entry(title='Test', content='Content Test')
        entry.save()
        request = self.client.delete(reverse_lazy('entry-delete', kwargs={'pk': 1}))
        self.assertEqual(request.status_code, status.HTTP_202_ACCEPTED)
