from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema

from entries.models import Entry
from entries.serializers import EntrySerializer


class EntryListView(APIView):

    def get(self, request):
        entries = Entry.objects.all().order_by("-date_created")
        serializer = EntrySerializer(entries, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class EntryDetailView(APIView):
    def get(self, request, pk):
        entry = Entry.objects.get(pk=pk)
        serializer = EntrySerializer(entry)
        return Response(serializer.data, status=status.HTTP_200_OK)


class EntryCreateView(APIView):
    @swagger_auto_schema(request_body=EntrySerializer)
    def post(self, request):
        serializer = EntrySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class EntryUpdateView(APIView):
    @swagger_auto_schema(request_body=EntrySerializer)
    def put(self, request, pk):
        entry = Entry.objects.get(pk=pk)
        serializer = EntrySerializer(entry, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)


class EntryDeleteView(APIView):

    def delete(self, request, pk):
        entry = Entry.objects.get(pk=pk)
        entry.delete()
        return Response(status=status.HTTP_202_ACCEPTED)
