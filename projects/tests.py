from rest_framework import status
from rest_framework.reverse import reverse_lazy
from rest_framework.test import APITestCase

from projects.models import Project


class ProjectIndexViewTest(APITestCase):
    def test_GET_request(self):
        project = Project(title='Random', description='description', technology='python')
        project.save()
        request = self.client.get(reverse_lazy('project_index'))
        self.assertEqual(request.status_code, status.HTTP_200_OK)


class ProjectDetailTest(APITestCase):

    def test_GET_request(self):
        project = Project(title='Random', description='description', technology='python')
        project.save()
        request = self.client.get(reverse_lazy('project_detail', kwargs={'pk': project.pk}))
        self.assertEqual(request.status_code, status.HTTP_200_OK)