from rest_framework import serializers


class ProjectSerializer(serializers.Serializer):
    title = serializers.CharField()
    description = serializers.CharField()
    technology = serializers.CharField()
    image = serializers.ImageField()