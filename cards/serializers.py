from rest_framework import serializers

from cards.models import Card


class CardCheckSerializer(serializers.Serializer):
    card_id = serializers.IntegerField(required=True)
    solved = serializers.BooleanField(required=False)

    def create(self, validated_data):
        card = Card(**validated_data)
        card.save()
        return card

    def update(self, instance, validated_data):
        instance.card_id = validated_data.get('card_id')
        instance.solved = validated_data.get('solved')
        instance.save()
        return instance


class BoxViewSerializer(serializers.Serializer):
    card_id = serializers.IntegerField(required=True)
    solved = serializers.BooleanField(required=False)

    def create(self, validated_data):
        card = Card(**validated_data)
        card.move(solved=validated_data.get('solved'))
        card.save()
        return card

    def to_representation(self, instance):
        reps = super().to_representation(instance)
        reps['box_number'] = self.validated_data.get('box_num')
        return reps
