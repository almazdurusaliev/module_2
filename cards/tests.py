from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse_lazy


class CardListViewTest(APITestCase):
    def test_GET_request(self):
        request = self.client.get(reverse_lazy('card-list'))
        self.assertEqual(request.status_code, status.HTTP_200_OK)
