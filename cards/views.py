from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Card
from .serializers import CardCheckSerializer, BoxViewSerializer


class CardListView(APIView):
    def get(self, request):
        cards = Card.objects.all().order_by("box", "-date_created")
        serializer = CardCheckSerializer(cards, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CardCreateView(APIView):
    @swagger_auto_schema(request_body=CardCheckSerializer)
    def post(self, request):
        serializer = CardCheckSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class CardUpdateView(APIView):
    @swagger_auto_schema(request_body=CardCheckSerializer)
    def put(self, request, pk):
        card = Card.objects.get(pk=pk)
        serializer = CardCheckSerializer(card, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class BoxView(APIView):
    def get(self, request, box_num):
        cards = Card.objects.filter(box=box_num)
        serializer = BoxViewSerializer(cards, many=True)
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(request_body=BoxViewSerializer)
    def post(self, request):
        serializer = BoxViewSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)
